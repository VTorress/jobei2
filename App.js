import React, { Component } from 'react';
import { Root, Tabs, createRootNavigator } from './app/config/router';
import {isSignedIn } from './app/config/auth';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: true
    };
  }

  compoonentDidMount() {
    isSignedIn()
      .then(res => this.setState({signedIn: res, checkedSignIn: true}))
      .catch(err => console.warn('um erro ocorreu'));
  }

  render() {
    const { checkedSignIn, signedIn } = this.state;


    if(!checkedSignIn) {
      return null
    }

    const Layout = createRootNavigator(signedIn)
    return <Layout />;
  }
}

export default App;

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
/**
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
//import {StackNavigator} from 'react-navigation';
import Teste from './screens/Teste';
import Main from './screens/Main';
import Login from './screens/Login';

import DetalhesVaga from './screens/DetalhesVaga';
import Cadastro from './screens/Cadastro';
import Habilidades from './screens/Habilidades';

import {AsyncStorage, ActivityIndicator} from 'react-native';
import {StackNavigator} from 'react-navigation';
/**const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});**/
//import SignIn from './screens/SignIn';
//import Logout from './screens/Logout';
/**import {createRootNavigator} from './router';
import {isSignedIn} from './auth';

import axios from 'axios';
//import {AppNavigator} from './AppNavigator';
const Navigator = StackNavigator({
  Teste,
  Main,
  Login,
  DetalhesVaga,
  Habilidades,
  Cadastro,

});


export default class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false,
    };
  }

  compoonentDidMount() {
    isSignedIn()
      .then(res => this.setState({signedIn: res, checkedSignIn: true}))
      .catch(err => alert('um erro ocorreu'));
  }

  getUser(sessionToken) {
    axios.get('http://3deaa84d.ngrok.io/api/candidatos',
      {headers: {'Content-Type': 'application/json',
      'api-version': '1.0'},
      'Authorization': sessionToken,
      }
    )
      .then(res => {
        console.warn(res.data);
        this.user = res.data;
      })
      .catch(error => {
        console.warn(error)
      });
  }

  render() {
    const { checkedSignIn, signedIn} = this.state;

    if(!checkedSignIn) {
      return null;
    }

    const Layout = createRootNavigator(signedIn);
    return <Layout />;

    /**if(signedIn) {
        return <SingedIn/>;
    } else {
      return <Logout />
    }**/
//  }
//}

    /**const accessToken = '1';

    if(accessToken){
      return (
        <Login />
        );
    } else {
        return(
          <Main />
        );
    }**/

    /**<View>
      <ActivityIndicator />
    </View>**/

    /**AsyncStorage.getItem('token')
      .then(token => {
        if(token) {
          return (
            <Main />
            );
          }else {
            return (
              <Login />
            );
          }
        })
        .catch(error => {
          return(<DetalhesVaga />)
    });**/
      //this.token = AsyncStorage.getItem('token');
      //this.user = this.getUser('Bearer ' + this.token);
      //console.warn(token);
      //console.warn(token);
      //this.user = getUser('Bearer ' + this.token.accessToken);
    //  AsyncStorage.setItem('@app.userId', user.id);
