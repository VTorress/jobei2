import {AsyncStorage} from 'react-native';

export const apiURI = 'http://192.168.4.165:5000/api';

export const headers = {
  headers: {
  'Content-Type': 'application/json',
  'api-version': '1.0',
  }
}

export const headersAuth = {
  headers: {
  'Content-Type': 'application/json',
  'api-version': '1.0',
  'Authorization': 'Bearer ' + this.token
  }
}
