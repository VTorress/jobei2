import React from 'react';
import { TabNavigator, StackNavigator, createSwitchNavigator } from 'react-navigation';
import {Platform} from 'react-native';
import { Icon } from 'react-native-elements';
import { Button} from 'native-base';

import Home from '../screens/Home';
import VagaDetail from '../screens/VagaDetail';
import EmpresaDetail from '../screens/EmpresaDetail';
import Perfil from '../screens/Perfil';
import Atividades from '../screens/Atividades';
import Conversas from '../screens/Conversas';

import Login from '../screens/Login';
import Cadastro from '../screens/Cadastro';
import Endereco from '../screens/Endereco';
import DadosPessoais from '../screens/DadosPessoais';
import DadosProfissionais from '../screens/DadosProfissionais';
import Habilidades from '../screens/Habilidades';
import Valores from '../screens/Valores';
import FinalCadastro from '../screens/FinalCadastro';

import Settings from '../screens/Settings';
import Senha from '../screens/Senha';
import EditarDados from '../screens/EditarDados';
import EditarFoto from '../screens/EditarFoto';
import EditarHabilidades from '../screens/EditarHabilidades';

export const CadastroStack = StackNavigator({
  Cadastro: {
    screen: Cadastro,
    navigationOptions: {
      title: 'Cadastro',
    }
  },
},
  {
    headerMode: 'none',
})

export const LoginStack = StackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      title: 'Login',
    },
  },
  Cadastro: {
    screen: Cadastro,
    navigationOptions: {
      title: 'Cadastro',
    },
  },
    DadosPessoais: {
      screen: DadosPessoais,
      navigationOptions: {
        title: 'Dados Pessoais',
      },
    },
    Endereco: {
      screen: Endereco,
      navigationOptions: {
        title: 'Endereço',
      },
    },
    DadosProfissionais: {
      screen: DadosProfissionais,
      navigationOptions: {
        title: 'Dados Profissionais',
      },
    },
    Habilidades: {
      screen: Habilidades,
      navigationOptions: {
        title: 'Habilidades Técnicas',
      },
    },
    Valores: {
      screen: Valores,
      navigationOptions: {
        title: 'Valores',
      },
    },
    FinalCadastro: {
      screen: FinalCadastro,
      navigationOptions: {
        title: 'Cadastro',
      },
    },
  },
  {
    //headerMode: 'auto',
})

export const HomeStack = StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: 'Vagas',
      //headerRight: <Button
      //  onPress={() => alert('this is a button')}
      //  ><Icon name='menu' type='Ionicons' size={35} color='gray' style={{backgroundColor:'white'}}/></Button>

    },
  },
  VagaDetails: {
    screen: VagaDetail,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.cargo}`,
    }),
  },
  EmpresaDetails: {
    screen: EmpresaDetail,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.nomeEmpresa}`,
    }),
  },
},
{
  //headerMode: 'none',
});

export const AtividadesStack = StackNavigator({
  Atividades: {
    screen: Atividades,
    navigationOptions: {
      title: 'Atividades',
    },
  },
  Conversas: {
    screen: Conversas,
    navigationOptions: {
      title: 'Conversas',
    }
  },
});

export const PerfilStack = StackNavigator({
  Perfil: {
    screen: Perfil,
    navigationOptions: ({ navigation }) => ({
      title: 'Perfil',
      headerRight:(
        <Button style={{backgroundColor: 'transparent', padding: 10, marginRight: 5}}
          onPress={() => navigation.navigate('Settings')}>
          <Icon name='menu' size={35} color='gray' />
        </Button>
      )
    })
  }
});

export const Tabs = TabNavigator({
  HomeStack: {
    screen: HomeStack,
    navigationOptions: {
      title: 'Vagas',
      tabBarIcon: ({ tintColor }) => <Icon name="home" size={35} color={tintColor} />
    }
  },
  AtividadesStatck: {
    screen: AtividadesStack,
    navigationOptions: {
      title: 'Atividades',
      tabBarIcon: ({ tintColor }) => <Icon name="favorite" size={35} color={tintColor} />
    },
  },
  PerfilStack: {
    screen: PerfilStack,
    navigationOptions: {
      title: 'Perfil',
      tabBarIcon: ({ tintColor }) => <Icon name="account-circle" size={35} color={tintColor} />,
    },
  },
},
{
  tabBarOptions : {
    style: {
     backgroundColor: (Platform.OS === 'android') ? 'rgb(0, 163, 151)' : 'white'
    }
  }
},
{
  headerMode: 'none',
});

export const SettingsStack = StackNavigator({

  Settings: {
    screen: Settings,
    navigationOptions: ({ navigation } ) => ({
      title: 'Configurações',
      headerRight:(
        <Button style={{backgroundColor: 'transparent', padding: 10, marginRight: 5, marginBottom: 5, flex: 2}}
          onPress={() => navigation.navigate('Perfil')}>
          <Icon name='arrow-downward' size={30} color='gray' />
        </Button>
      )
    }),
  },

  Senha: {
    screen: Senha,
    navigationOptions: {
      title: 'Alterar senha',
    },
  },

  EditarDados: {
    screen: EditarDados,
    navigationOptions: {
      title: 'Editar Dados',
    },
  },

  EditarFoto: {
    screen: EditarFoto,
    navigationOptions: {
      title: 'Editar Foto',
    },
  },

  EditarHabilidades: {
    screen: EditarHabilidades,
    navigationOptions: {
      title: 'Editar Habilidades',
    },
  },
});

export const UserLogedIn = StackNavigator({
  Tabs: {
    screen: Tabs,
  },
  Settings: {
    screen: SettingsStack,
  },
}, {
  mode: 'modal',
  headerMode: 'none',
});

export const UserLogedOut = StackNavigator({
  LoginStack: {
    screen: LoginStack,
  },
}, {
  mode: 'modal',
  headerMode: 'none',
});

export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      UserLogedIn: {
        screen: UserLogedIn
      },
      UserLogedOut: {
        screen: UserLogedOut,
      },
    },
    {
      initialRouteName: signedIn ? 'UserLogedIn' : 'UserLogedOut'

  /**LoginStack: {
    screen: LoginStack,
  },
  Tabs: {
    screen: Tabs,
  },
  Settings: {
    screen: SettingsStack,
  },*/
  }, {
    mode: 'modal',
    headerMode: 'none',
  });
}
