import React, { Component } from 'react';

import {Container, Content, Header, Body,
        List, ListItem, Thumbnail, Right, Left, Button} from 'native-base';

import {Platform, StyleSheet, Text, View,
        Image, AsyncStorage} from 'react-native';

import Base64 from "../config/Base64";
import axios from "axios";
import {apiURI} from '../config/JobeiApi';
import { messages } from '../config/data';

const token = "";
const idCandidato = "";

export default class Atividades extends Component {

  constructor(props) {
    super(props);

    this.state = {
      curtidas: [],
      matches: [],
    }
  }

  async componentDidMount() {
    await this.init();
      console.warn(this.idCandidato);
      axios.get(apiURI + '/curtidas/candidato/' + this.idCandidato,
        {headers: {
          'Content-Type': 'application/json',
          'api-version': '1.0',
      }})
      .then(res => {
        let curtidasApi = res.data
        this.setState({curtidas: curtidasApi});
      })
      .catch(error => {
        showToastTop("Não foi possível carregar as curtidas. O servidor está em manutenção. Tente novamente mais tarde.");
      });

      axios.get(apiURI + '/curtidas/candidato/' + this.idCandidato + '/matches',
        {headers: {
          'Content-Type': 'application/json',
          'api-version': '1.0',
      }})
      .then(res => {
        let matchesApi = res.data
        this.setState({matches: matchesApi});
      })
      .catch(error => {
        showToastTop("Não foi possível carregar os matches. O servidor está em manutenção. Tente novamente mais tarde.");
      });
  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
    //console.warn(this.parseJwt(this.token).sub);
  }

  render() {
    return (
      <Content style={{backgroundColor: 'white', padding: 20}}>
        <View style={styles.container}>

        <View>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Curtidas</Text>
            {this.state.curtidas.lenght === 0 ?
              (<Text style={{marginTop: 5}}>Você nào tem nenhuma curtida ate o momento</Text>)
              :
            (<View style={styles.curtidas}>
              {this.state.curtidas.map((curtida) => (
                <Thumbnail source={{uri: curtida.vaga.caminhoFotoEmpresa}} style={styles.foto} />
              ))}
          </View>)}
        </View>

          <View style={styles.lineStyle}></View>

          <View>
            <Text style={{fontWeight: 'bold', fontSize: 16, marginTop: 10}}>Matches</Text>
          </View>

          <View>
            {this.state.matches.lenght === 0 ?
              (<Text style={{marginTop: 5}}>Você nao tem nenhum matche ate o momento</Text>)
              :

              (<List>
                {this.state.matches.map(matche =>
                <ListItem avatar key={matche.id}
                  onPress={() => this.props.navigation.navigate('Conversas')}>
                  <Left>
                      <Thumbnail small source={{uri: matche.vaga.caminhoFotoEmpresa}} />
                  </Left>
                  <Body>
                    <Text style={{fontWeight: 'bold'}}>{matche.vaga.nomeEmpresa}</Text>
                    <Text note>{matche.vaga.cargo}</Text>
                  </Body>
                  <Right>
                    <Text note>18:30 PM</Text>
                  </Right>
                </ListItem>
              )}
              </List>)
            }

          </View>
        </View>
    </Content>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    marginTop: 2,
    padding: 23,
  },
  principal: {
    alignItems: 'center',
  },
  curtidas: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  cabecalho: {
    flexDirection: 'row',
  },
  dadosEmpresa: {
    width: 120,
  },
  dadosVaga: {
    marginTop: 50
  },
  curtir: {
    marginTop: 10,
    alignItems: 'flex-end',
  },
  descricao: {
    marginTop: 30,
    marginBottom: 20,
    //alignItems: 'center',
  },
  valor: {
    marginLeft: 10,
  },
  titulo: {
    fontWeight: 'bold',
    marginTop: 10,
  },
  foto2: {
    resizeMode: 'contain',
    width: 100,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  foto: {
    resizeMode: 'contain',
    width: 60,
    height: 60,
    marginTop: 5,
    marginLeft: 15,
    borderWidth: 1,
    borderColor: 'gray',
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
});
