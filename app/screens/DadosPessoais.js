import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, PixelRatio, Alert } from 'react-native';
import { Root } from 'native-base';
import { cargos, imagem_padrao } from '../config/data';
import ImagePicker from 'react-native-image-picker';
import {showToastBottom, showToastTop} from '../utils/helpers';

export default class DadosPessoais extends Component{
    constructor(props) {
        super(props);
    
        this.state = {
          telefone: '',
          telefoneError: false,
          email: '',
          emailError: false,
          senha: '',
          senhaError: false,
        }
    }    

    goToScreen = (user) => {

        if(this.state.telefoneError === false
         && this.state.emailError === false && this.state.senhaError === false) {
        
        this.props.navigation.navigate('Endereco',
          {
            nome: this.state.nome,
            sobrenome: this.state.sobrenome,
            email: this.state.email,
            senha: this.state.senha,
            foto: this.state.foto,
            avatarSource: this.state.avatarSource,
            telefone: this.state.telefone
          });
        } else {
         showToastTop('Preencha os dados corretamente antes de continuar');
        }
    };

    validate(texto, type){
        telefone = /^[(]{0,9}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-/\s\.]{0,1}[0-9]{4}$/
        email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        senha = /^(?=.{6,}$)/
        if(type == 'telefone'){
          if(telefone.test(texto)){
            this.setState({
                telefoneError: false,
            })
    
          }
          else{
            this.setState({
                telefoneError: true,
            })
            showToastTop('Telefone Inválido');
          }
        }
        else if (type == 'email') {
          if (email.test(texto)) {
            this.setState({
              emailError: false,
            })
          }
          else {
            this.setState({
              emailError: true,
            })
            showToastBottom('Entre com um email válido');
          }
        }
        else if (type == 'senha') {
          if (senha.test(texto)) {
            this.setState({
              senhaError: false,
            })
          }
          else {
            this.setState({
              senhaError: true,
            })
            showToastBottom('A senha deve conter mais de 6 caracteres');
          }
        }
    }

    render() {
        return(
            <Root>
                <View style={styles.container}>
                    <View style={styles.titulo}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Dados Pessoais</Text>

                        <View style={{ flexDirection: 'row', marginLeft: '38%' }}>
                            <TouchableOpacity onPress={() => this.goToScreen(this.props.navigation.state.params)}>
                            <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(0, 163, 151)' }}>continue</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.lineStyle} />

                    <View style={styles.inputContainer}>

                        <TextInput placeholder='Telefone' style={[styles.inputs,
                        this.state.telefoneError ? styles.erroBorder : null]}
                        onChangeText={(telefone) => this.setState({ telefone })}
                        value={this.state.telefone}
                        autoCapitalize='none'
                        keyboardType={'phone-pad'}
                        onEndEditing={() => this.validate(this.state.telefone, 'telefone')}
                        />

                        <TextInput placeholder='E-mail' style={[styles.inputs,
                        this.state.emailError ? styles.erroBorder : null]}
                        onChangeText={(email) => this.setState({ email })}
                        value={this.state.email}
                        autoCapitalize='none'
                        onEndEditing={() => this.validate(this.state.email, 'email')}
                        />

                        <TextInput placeholder='Senha' style={[styles.inputs,
                        this.state.senhaError ? styles.erroBorder : null]}
                        onChangeText={(senha) => this.setState({ senha })}
                        value={this.state.senha}
                        autoCapitalize='none'
                        secureTextEntry={true}
                        onEndEditing={() => this.validate(this.state.senha, 'senha')}
                        />
                    
                    </View>
                </View>
            </Root>
        );
    }
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 40,
      backgroundColor: 'white',
    },
    lineStyle: {
      marginTop: '3%',
      borderBottomColor: 'rgb(58, 203, 198)',
      borderBottomWidth: 1
    },
    inputContainer: {
      alignItems: 'center'
    },
    inputs: {
      paddingBottom: 5,
      marginTop: 20,
      textAlign: 'center',
      height: 40,
      //borderColor: '#8a8c91',
      borderColor: '#9B9B9B',
      //borderColor: 'rgb(58, 203, 198)',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      width: '90%',
      borderRadius: 5
    },
    botao: {
      backgroundColor: 'rgb(58, 203, 198)',
      marginTop: 20
    },
    titulo: {
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
    erroBorder: {
      borderWidth: 1,
      borderColor: 'red',
    },
    erroText: {
      color: 'red',
    },
  });