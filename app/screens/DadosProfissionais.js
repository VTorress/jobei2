import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image, TextInput,
  AsyncStorage, TouchableOpacity} from 'react-native';

import {Container, Content, Header, Body, Title, Toast, Root,
  Picker, Form, Icon, Textarea, Left, Right, Button,} from 'native-base';

import axios from 'axios';
//import {getEnderecoApi} from '../config/restApi';
import MultiSelect from 'react-native-multiple-select';
//import { cargos } from '../config/data';
import {showToastTop, showToastBottom} from '../utils/helpers';
import {apiURI} from '../config/JobeiApi';

export default class DadosProfissionais extends Component {

  constructor(props){
    super(props);

    this.state = {
      descricao: '',
      descricaoError: false,
      cargo: undefined,
      cargos: [],
    }
  }

  onValueChange(value: string) {
     this.setState({
       cargo: value
     })
  }

  async componentDidMount() {
      axios.get(apiURI + '/cargos',
        {headers: {
          'Content-Type': 'application/json',
          'api-version': '1.0',
      }})
      .then(res => {
        let cargosApi = res.data
        console.warn(cargosApi);
        //this.vagasApi = vagas
        this.setState({cargos: cargosApi});
      })
      .catch(error => {
        //console.warn(error.message);
        showToastTop("Não foi possível carregar os cargos. O servidor está em manutenção. Tente novamente mais tarde.");
      });
    }

  validate(texto, type){
    if(type == 'descricao'){
      if(this.state.descricao.length >= 5 && this.state.descricao.length <= 300){
        this.setState({
        descricaoError: false,
        })
      }
      else {
        this.setState({
          descricaoError: true,
        })
        showToastTop('A descrição deve ter entre 5 e 300 caracteres')
      }
    }
  }

  goToScreen = (user) => {
      this.props.navigation.navigate('Habilidades', {
        foto: user.foto,
        nome: user.nome,
        sobrenome: user.sobrenome,
        email: user.email,
        senha: user.senha,
        avatarSource: user.avatarSource,
        telefone: user.telefone,
        endereco: user.endereco,
        cargo: this.state.cargo,
        descricao: this.state.descricao,
        });
  };

  render(){

      //const { foto, nome, sobrenome, email, senha, cargo, descricao, endereco} = this.props.navigation.state.params;

      const cargosOrdenados = this.state.cargos.sort(function(a, b){
          return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
      });


      return(
      <Root>
        <Content style={{backgroundColor: 'white'}}>
          <ScrollView style={styles.container}>

              <View style={styles.titulo}>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>Dados Profissionais</Text>

                <View style={{flexDirection: 'row', marginLeft: '28%'}}>
                  <TouchableOpacity onPress = {() => this.goToScreen(this.props.navigation.state.params)}>
                  <Text style={{fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(0, 163, 151)'}}>continue</Text>
                  </TouchableOpacity>
                </View>
              </View>


              <View style={styles.lineStyle} />

              <View style={styles.inputContainer}>

                <View style={styles.inputs}>
                   <Form>
                     <Picker
                      renderHeader={backAction =>
                        <Header>
                        <Left>
                          <Button transparent onPress={backAction}>
                            <Icon name="arrow-back" style={{ color: "#007aff" }} />
                          </Button>
                        </Left>
                        <Body style={{alignItems:'center'}}><Title>Cargo</Title></Body>
                        <Right/>
                      </Header>}
                      mode="dropdown"
                       //iosIcon={<Icon name="ios-arrow-down-outline" />}
                       style={{ width: undefined, textAlign: 'center'}}
                       headerBackButtonText="voltar"
                       placeholder="Selecione o Cargo"
                       placeholderStyle={{ color: '#CCC'}}
                       placeholderIconColor="#CCC"
                       textStyle={{ fontSize: 14, textAlign: 'center', marginBottom: '5%' }}
                       selectedValue={this.state.cargo}
                       onValueChange={this.onValueChange.bind(this)}
                     >
                     {cargosOrdenados.map((cargo) => (
                       <Picker.Item label={cargo.nome} value={cargo.nome}/>
                     ))}
                     </Picker>
                   </Form>
                 </View>


                 <Form style={{width: '90%'}}>
                  <Textarea rowSpan={5} placeholder="Fale um pouco sobre você..."
                  placeholderTextColor='#CCC'
                  style={[styles.textArea,
                  this.state.descricaoError? styles.erroBorder: null]}
                  onChangeText={(descricao) => this.setState({descricao})}
                  onEndEditing={() => this.validate(this.state.descricao, 'descricao')}
                  value={this.state.descricao}
                  autoCapitalize='none'/>
                 </Form>

            </View>
          </ScrollView>
        </Content>
        </Root>
      );
  }

}
  const styles = StyleSheet.create({
    container: {
      //flex: 1,
      //justifyContent: 'center',
      //alignItems: 'center',
      padding: 40,
    },
    lineStyle: {
      //marginTop: 20,
      //marginBottom: 20,
      marginTop: '3%',
      borderBottomColor: 'rgb(0, 163, 151)',
      borderBottomWidth: 1
    },
    inputContainer: {
      alignItems: 'center'
    },
    inputs: {
      paddingBottom: 5,
      marginTop: 20,
      height: 40,
      borderColor: '#8a8c91',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      width: '90%',
      borderRadius: 5
    },
    botao: {
      backgroundColor: 'rgb(0, 163, 151)',
      marginTop: 20
    },
    titulo: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    lista: {
      height: 150,
    },
    containerLista:{
      flex: 1,
      margin: 15,
      padding: 30,
      flexDirection: 'row',
      //alignItems: 'center',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: 'rgb(0, 163, 151)',
    },
    erroBorder:{
      borderWidth: 1,
      borderColor: 'red',
    },
    textArea: {
      marginTop: 20,
      borderColor: '#8a8c91',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      height: 120,
      borderRadius: 5,
      fontSize: 14,

    },

  });
