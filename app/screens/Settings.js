import React, { Component } from 'react';
import { ScrollView, AsyncStorage } from 'react-native';
import { List, ListItem } from 'react-native-elements';

import {onSignOut} from '../config/auth';

class Settings extends Component {

  async logout(){
    await AsyncStorage.removeItem('token');
    this.props.navigation.navigate('UserLogedOut');
  }

  render() {
    return (
      <ScrollView>

        <List>
          <ListItem
            title="Editar Foto"
            onPress={() => this.props.navigation.navigate('EditarFoto')}
            rigthIcon={{name: 'arrow-downward'}}
          />
          <ListItem
            title="Editar Dados"
            onPress={() => this.props.navigation.navigate('EditarDados')}
            rigthIcon={{name: 'arrow-downward'}}
          />

          <ListItem
            title="Editar Habilidades"
            onPress={() => this.props.navigation.navigate('EditarHabilidades')}
          />
          <ListItem
            title="Editar Valores"
            onPress={() => this.props.navigation.navigate('Atividades')}
          />
        </List>

        <List>
          <ListItem
            title='Alterar senha'
            onPress={() => this.props.navigation.navigate('Senha')}
            />

          <ListItem
            title="Logout"
            onPress={() => this.logout()}
            rightIcon={{ name: 'cancel' }}
          />
        </List>
      </ScrollView>
    );
  }
}

export default Settings;
