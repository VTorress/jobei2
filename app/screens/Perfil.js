import React, { Component } from "react";
import {
  Container,
  Content,
  Header,
  Body,
  List,
  ListItem,
  Thumbnail,
  Badge,
  Root,
} from "native-base";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  AsyncStorage,
} from "react-native";
//import { Tile, List, ListItem, Button } from 'react-native-elements';
import { me } from "../config/data";

import Base64 from "../config/Base64";

import axios from "axios";
import {apiURI} from '../config/JobeiApi';

const token = "";
const idCandidato = "";

export default class Perfil extends Component {
  // handleSettingsPress = () => {
  //   this.props.navigation.navigate("Settings");
  // };

  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      sobrenome: '',
      email: '',
      descricao: '',
      cargo: [],
      habilidades: [],
      valores: [],
      imagem: '',
    }
  }


  showToast(message) {
      return (
        Toast.show({
          text: message,
          buttonText: 'OK',
          duration: 5000,
          position: 'bottom',
          textStyle: {color: 'white'},
          type: 'warning'
        })
      );
  }

  async componentDidMount() {
    await this.init();
    let perfilApi = null;

    axios.get( apiURI + '/candidatos/' + this.idCandidato,
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + this.token
    }})
    .then(res => {
      perfilApi = res.data;
      this.setState({nome: perfilApi.nome});
      this.setState({sobrenome: perfilApi.sobrenome});
      this.setState({endereco: perfilApi.endereco});
      this.setState({email: perfilApi.email});
      this.setState({descricao: perfilApi.descricao});
      this.setState({cargo: perfilApi.cargo});
      this.setState({habilidades: perfilApi.habilidades});
      this.setState({valores: perfilApi.valores});
      this.setState({imagem: perfilApi.imagem});
    })
    .catch(error => {
      this.showToast('Não foi possível carregar o Perfil. Tente novamente mais tarde');
    });
  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
    //console.warn(this.parseJwt(this.token).sub);
  }

  _retrieveData = async () => {
    try {
      this.token = await AsyncStorage.getItem("token");
      if (this.token !== null) {
        return this.token;
      }
    } catch (error) {
      console.warn("um erro ocorreu");
    }
  };

  render() {

    const imagemApi = <Thumbnail large source={{uri: this.state.imagem}} />;
    const padrao = <Thumbnail large source={require('../resources/img/padrao.jpg')} />;

    return (
      <Root>
      <Content style={{ backgroundColor: "white" }}>
        <List>
          <ListItem itemHeader first style={{ alignItems: "center" }}>

            <View><Text>{this.state.imagem == null ? padrao : imagemApi}</Text></View>

            <View style={{ flexDirection: "column" }}>
              <Text
                style={{ fontWeight: "bold", fontSize: 16, marginLeft: 20 }}
              >
                {this.state.nome + " " + this.state.sobrenome}
              </Text>
              <Text key={this.state.cargo.id} style={{ marginLeft: 20 }}>{this.state.cargo.nome}</Text>
            </View>
          </ListItem>

          <ListItem itemHeader>
            <Text style={{ color: "rgb(0, 163, 151)" }}>DADOS</Text>
          </ListItem>
          <ListItem>
            <Text>{this.state.endereco}</Text>
          </ListItem>
          <ListItem>
            <Text>{this.state.email}</Text>
          </ListItem>
          <ListItem itemHeader>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ color: "rgb(0, 163, 151)" }}>HABILIDADES</Text>
              <Text style={{ color: "rgb(0, 163, 151)", marginLeft: 60 }}>
                VALORES
              </Text>
            </View>
          </ListItem>

          <ListItem>
            <View style={{ flexDirection: "row" }}>
              <View>
                {this.state.habilidades.map(habilidade => (
                  <Badge
                    style={{
                      backgroundColor: "rgb(0, 163, 151)",
                      borderBottomLeftRadius: 5,
                      borderBottomRightRadius: 5,
                      borderTopLeftRadius: 5,
                      borderTopRightRadius: 5,
                      marginTop: 5,
                      flexDirection: "row"
                    }}
                  >
                    <Text key={habilidade.id} style={{ color: "white" }}>{habilidade.nome}</Text>
                  </Badge>
                ))}
              </View>

              <View style={{ marginLeft: 60 }}>
                {this.props.valores.map(valor => (
                  <Badge
                    style={{
                      backgroundColor: "rgb(0, 163, 151)",
                      borderBottomLeftRadius: 5,
                      borderBottomRightRadius: 5,
                      borderTopLeftRadius: 5,
                      borderTopRightRadius: 5,
                      marginTop: 5,
                      flexDirection: "row"
                    }}
                  >
                    <Text key={valor.id} style={{ color: "white" }}>{valor.nome}</Text>
                  </Badge>
                ))}
              </View>
            </View>
          </ListItem>

        </List>
      </Content>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 55,
    flexDirection: "row"
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: "gray",
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: "center"
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: "center",
    height: 40,
    borderColor: "#8a8c91",
    backgroundColor: "#ffffff",
    borderWidth: 1,
    width: "90%",
    borderRadius: 5
  },
  botao: {
    backgroundColor: "rgb(0, 163, 151)",
    marginTop: 20
  }
});

Perfil.defaultProps = { ...me };
