import React, { Component } from 'react';

import { Platform, StyleSheet, Text, View, ScrollView, Image,
  TextInput, AsyncStorage, TouchableOpacity, PixelRatio, Alert } from 'react-native';

import { Container, Content, Header, Body, Toast, Root, Picker,
  Form, Icon, Textarea, Left, Right, Button, Title } from 'native-base';

import AppIntroSlider from 'react-native-app-intro-slider';
import ImagePicker from 'react-native-image-picker';
import MultiSelect from 'react-native-multiple-select';

import Base64 from "../config/Base64";
import {apiURI} from '../config/JobeiApi';
import axios from 'axios';
import { habilidades, cargos, imagem_padrao } from '../config/data';
import { onSignIn } from "../config/auth";
import {showToastBottom, showToastTop} from '../utils/helpers';

const token = "";
const idCandidato = "";

export default class EditarDados extends Component {

  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      nomeError: false,
      sobrenome: '',
      sobrenomeError: false,
      email: '',
      emailError: false,
      senha: '',
      senhaError: false,
      endereco: '',
      enderecoError: false,
      cep: '',
      cepError: false,
      numero: '',
      numeroError: false,
      descricao: '',
      descricaoError: false,
      cargo: {},
      cargos: [],
    }
  }

  onValueChange(value: string) {
    this.setState({
      cargo: value
    })
  }

  async componentDidMount() {
    await this.init();
    let perfilApi = null;

      axios.get(apiURI + '/cargos',
        {headers: {
          'Content-Type': 'application/json',
          'api-version': '1.0',
      }})
      .then(res => {
        let cargosApi = res.data
        console.warn(cargosApi);
        //this.vagasApi = vagas
        this.setState({cargos: cargosApi});
      })
      .catch(error => {
        //console.warn(error.message);
        showToastTop("Não foi possível carregar os cargos. O servidor está em manutenção. Tente novamente mais tarde.");
      });

      axios.get( apiURI + '/candidatos/' + this.idCandidato,
      {
        headers: {
        'Content-Type': 'application/json',
        'api-version': '1.0',
        'Authorization': 'Bearer ' + this.token
      }})
      .then(res => {
        perfilApi = res.data;
        this.setState({nome: perfilApi.nome});
        this.setState({sobrenome: perfilApi.sobrenome});
        this.setState({endereco: perfilApi.endereco});
        this.setState({email: perfilApi.email});
        this.setState({descricao: perfilApi.descricao});
        this.setState({cargo: perfilApi.cargo});
      })
      .catch(error => {
        this.showToast('Não foi possível carregar os dados do usuário. Tente novamente mais tarde');
      });

    }

    parseJwt(token) {
      var base64Url = token.split(".")[1];
      var base64 = base64Url.replace("-", "+").replace("_", "/");
      return JSON.parse(Base64.atob(base64));
    }

    async init() {
      this.token = await AsyncStorage.getItem("token");
      this.idCandidato = this.parseJwt(this.token).sub;
      //console.warn(this.parseJwt(this.token).sub);
    }

    getEndereco = (cep, numero) => {
      axios.get('https://viacep.com.br/ws/'+cep+'/json/')
      .then(res => {
        enderecoJSON = res.data;
        if (enderecoJSON.logradouro === undefined){
          this.setState({enderecoError: true});
          showToastTop('Não foi possível encotrar o endereço com este CEP. Tente novamente.');
        } else {
          this.setState({enderecoError: false});
          this.setState({endereco: enderecoJSON.logradouro + ", " + numero +
            " - " + enderecoJSON.localidade +  ", " + enderecoJSON.uf});
       }
      })
      .catch(error => {
        showToastTop("Não foi possível localizar o endereço. O servidor está em manutenção. Tente novamente mais tarde");
        //console.warn(error.message);
      });
    }


  render() {

    const cargosOrdenados = this.state.cargos.sort(function(a, b){
        return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
    });

    return (
    <Root>
    <ScrollView>

        <View style={styles.container}>
        <View style={styles.titulo}>
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Dados Pessoais</Text>
        </View>

        <View style={styles.lineStyle} />

        <View style={styles.inputContainer}>
          <TextInput placeholder={this.state.nome} style={[styles.inputs,
          this.state.nomeError ? styles.erroBorder : null]}
            onChangeText={(nome) => this.setState({ nome })}
            onEndEditing={() => this.validate(this.state.nome, 'nome')}
            value={this.state.nome} />

          <TextInput placeholder={this.state.sobrenome} style={[styles.inputs,
          this.state.sobrenomeError ? styles.erroBorder : null]}
            onChangeText={(sobrenome) => this.setState({ sobrenome })}
            value={this.state.sobrenome}
            onEndEditing={() => this.validate(this.state.sobrenome, 'sobrenome')}
          />

          <TextInput placeholder={this.state.email} style={[styles.inputs,
          this.state.emailError ? styles.erroBorder : null]}
            onChangeText={(email) => this.setState({ email })}
            value={this.state.email}
            autoCapitalize='none'
            onEndEditing={() => this.validate(this.state.email, 'email')}
          />

          </View>


          <View style={styles.titulo2}>
            <Text style={{fontSize: 16, fontWeight: 'bold'}}>Endereço</Text>
          </View>

            <View style={styles.lineStyle} />

          <View style={styles.inputContainer}>
          <TextInput placeholder='endereço' style={styles.inputsEndereco}
            onChangeText={texto => this.setState({endereco: texto})}
            autoCapitalize='none'
            editable={false}
            value={this.state.endereco}
            />

            <TextInput placeholder='cep' style={[styles.inputs,
             this.state.cepError? styles.erroBorder: null]}
              onChangeText={(cep) => this.setState({cep})}
              onEndEditing={() => this.validate(this.state.cep, 'cep')}
              value={this.state.cep}
              autoCapitalize='none' />

            <TextInput placeholder='numero' style={[styles.inputs,
             this.state.numeroError? styles.erroBorder: null]}
              onChangeText={(numero) => this.setState({numero})}
              onEndEditing={() => this.validate(this.state.numero, 'numero')}
              value={this.state.numero}
              keyboardType={'number-pad'}
              />

              <TouchableOpacity onPress = {() => this.getEndereco(this.state.cep, this.state.numero)}>
                <View style={{borderColor: 'rgb(0, 163, 151)', borderRadius: 5, borderWidth: 1,
                marginTop: 20, width: '90%'}}>
                  <Text style={{fontSize: 12, padding: 10, height: 40, fontWeight: 'bold', color: 'rgb(0, 163, 151)',
                  textAlign: 'center', paddingBottom: 5}}>
                    TROCAR ENDEREÇO
                  </Text>
                </View>
              </TouchableOpacity>

          </View>

          <View style={styles.titulo2}>
            <Text style={{fontSize: 16, fontWeight: 'bold'}}>Dados Profissionais</Text>
          </View>

            <View style={styles.lineStyle} />
          <View style={styles.inputContainer}>

          <View style={styles.inputs}>
             <Form>
               <Picker
                renderHeader={backAction =>
                  <Header>
                  <Left>
                    <Button transparent onPress={backAction}>
                      <Icon name="arrow-back" style={{ color: "#007aff" }} />
                    </Button>
                  </Left>
                  <Body style={{alignItems:'center'}}><Title>Cargo</Title></Body>
                  <Right/>
                </Header>}
                mode="dropdown"
                 //iosIcon={<Icon name="ios-arrow-down-outline" />}
                 style={{ width: undefined, textAlign: 'center'}}
                 headerBackButtonText="voltar"
                 placeholder={this.state.cargo.nome}
                 placeholderStyle={{ color: '#000'}}
                 placeholderIconColor="#CCC"
                 textStyle={{ fontSize: 14, textAlign: 'center', marginBottom: '5%' }}
                 selectedValue={this.state.cargo}
                 onValueChange={this.onValueChange.bind(this)}
               >
               {cargosOrdenados.map((cargo) => (
                 <Picker.Item label={cargo.nome} value={cargo.nome}/>
               ))}
               </Picker>
             </Form>
           </View>


           <Form style={{width: '90%'}}>
            <Textarea rowSpan={5} placeholder="Fale um pouco sobre você..."
            placeholderTextColor='#000'
            style={[styles.textArea,
            this.state.descricaoError? styles.erroBorder: null]}
            onChangeText={(descricao) => this.setState({descricao})}
            onEndEditing={() => this.validate(this.state.descricao, 'descricao')}
            value={this.state.descricao}
            autoCapitalize='none'/>
           </Form>

        </View>

      </View>

      </ScrollView>
      </Root>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    backgroundColor: 'white',
  },
  lineStyle: {
    marginTop: '3%',
    borderBottomColor: 'rgb(58, 203, 198)',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center',
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    //borderColor: '#8a8c91',
    borderColor: '#9B9B9B',
    //borderColor: 'rgb(58, 203, 198)',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  inputsEndereco: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    backgroundColor: '#ffffff',
    borderBottomColor: '#9B9B9B',
    width: '90%',
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  },
  titulo: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  titulo2: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 30,
  },
  erroBorder: {
    borderWidth: 1,
    borderColor: 'red',
  },
  erroText: {
    color: 'red',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  textArea: {
    marginTop: 20,
    borderColor: '#8a8c91',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    height: 120,
    borderRadius: 5,
    fontSize: 14,

  },
});
