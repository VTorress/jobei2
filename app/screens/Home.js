import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  Button,
  AsyncStorage,
  TouchableOpacity,
  Platform
} from 'react-native';

import { List, ListItem, Card, Icon , Avatar} from 'react-native-elements';
import {Container, Content} from 'native-base';

import { vagas } from '../config/data';
import {apiURI} from '../config/JobeiApi';

import axios from 'axios';

const token = '';

class Home extends Component {

  constructor(props) {
    super(props)

    this.state = {
      vagasApi: []
    }
  }

  async componentDidMount() {
    await this.init()

    axios.get( apiURI + '/candidatos/vagas/',
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + this.token
    }})
    .then(res => {
      let vagas = res.data
      this.setState({vagasApi : vagas});
    })
    .catch(error => {
      console.warn(error.message);
    });
  }

    //this.state.setState({vagasApi : this.getVagas()});
    //console.warn(this.state.vagasApi)
  //}

  async init() {
      this.token = await AsyncStorage.getItem('token')
  }


  goToScreen = (vaga) => {
    this.props.navigation.navigate('VagaDetails', { ...vaga });
  };

  _retrieveData = async () => {
  try {
    this.token = await AsyncStorage.getItem('token');
    //console.warn(this.token);
    if (this.token !== null) {
      return this.token;
    }
   } catch (error) {
     console.warn('um erro ocorreu');
   }
 }


  render() {

    const curtida = <Image source={require('./../resources/img/s2-checked.png')}
      style={styles.curtir} />
    const naoCurtida = <Image source={require('./../resources/img/s2.png')}
      style={styles.curtir} />

    const vagasOrdenadas = this.state.vagasApi.sort(function(a, b){
        return a.porcentagem > b.porcentagem ? -1 : a.porcentagem < b.porcentagem ? 1 : 0;
      });

    return (
      <Content style={{backgroundColor: 'white'}}>

        <List>
          {vagasOrdenadas.map((vaga) => (
            /*<ListItem
              key={vaga.id}
              roundAvatar
              avatar={require('./../resources/img/google.jpg')}
              title={`${vaga.titulo}`}
              subtitle={vaga.descricao}
              onPress={() => this.onLearnMore(vaga)}
            />*/

            <View style={{alignItems: 'center'}}>
            <TouchableOpacity onPress={() => this.goToScreen(vaga)}>
              <Card
                containerStyle={{width: Dimensions.get('window').width - 100}}
                >
                <View>

                  <View style={{alignItems: 'flex-start'}}>
                      <Text>{vaga.nomeEmpresa}</Text>
                  </View>


                  <View style={styles.imgContainer}>
                    <Image
                        style={styles.foto}
                        source={{uri: vaga.imagem}}
                        />
                  </View>


                  <View style={styles.dados}>
                      <Text style={styles.descricao}>
                        {vaga.cargo}
                      </Text>
                      <Text style={styles.porcentagem}>
                         {vaga.porcentagem}
                      </Text>
                  </View>

                  <View style={{alignItems: 'flex-start'}}>
                    <View style={{margintTop: 20}}>
                      <Text>
                          {vaga.descricao}
                        </Text>
                    </View>
                  </View>
              </View>


                { Platform.OS === 'android'? 
                (<View style={{alignItems: 'flex-end',height: 20}}>
                <View style={{height:25}}>{vaga.curtiu ? curtida : naoCurtida}</View>
                </View>
                )
                :
                (<View style={{alignItems: 'flex-end'}}>
                <Text>{vaga.curtiu ? curtida : naoCurtida}</Text>
              </View>)
              }
                



            </Card>
            </TouchableOpacity>
          </View>
          ))}
        </List>
      </Content>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  container:{
    alignItems: 'center'
  },
  imgContainer:{
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  foto: {
    height: 60,
    width: 60,
    borderRadius: 20,

  },
  dados: {
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  descricao:{
    alignItems: 'flex-start',
    marginBottom: 10,
    fontWeight: 'bold'
  },
  porcentagem:{
    marginLeft: 10,
    color: 'rgb(0, 163, 151)',
    fontWeight: 'bold'
  },
  curtir: {
    resizeMode: 'contain',
    
    marginLeft: 12,
    width: 20,
    height: 20,
    

  },
});
