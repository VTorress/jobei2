import React, { Component } from 'react';

import {Platform, StyleSheet, Text, View, ScrollView, Image,
  TextInput, AsyncStorage, Button, TouchableOpacity} from 'react-native';

import {Container, Content, Header, Body, Toast, Root, Picker,
  Form, Icon} from 'native-base';

import axios from 'axios';
//import {getEnderecoApi} from '../config/restApi';
//import { habilidades } from '../config/data';
import { onSignIn } from "../config/auth";
import MultiSelect from 'react-native-multiple-select';
import {apiURI} from '../config/JobeiApi';
import {showToastTop, showToastBottom} from '../utils/helpers';
import Base64 from "../config/Base64";

const token = "";
const idCandidato = "";

export default class EditarHabilidades extends Component {

  constructor(props){
    super(props);

    this.state = {
      habilidades: [],
      selectedItems: [],
    }
  }

  onSelectedItemsChange = selectedItems => {
   this.setState({ selectedItems });
  };

  async componentDidMount() {
    await this.init();
    let perfilApi = null;

    axios.get( apiURI + '/candidatos/' + this.idCandidato,
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + this.token
    }})
    .then(res => {
      perfilApi = res.data;
      this.setState({selectedItems: perfilApi.habilidades});
      console.warn(this.state.selectedItems);
    })
    .catch(error => {
      this.showToast('Não foi possível carregar os dados do usuário. Tente novamente mais tarde');
    });

      axios.get(apiURI + '/habilidades',
        {headers: {
          'Content-Type': 'application/json',
          'api-version': '1.0',
      }})
      .then(res => {
        let habilidadesApi = res.data
        this.setState({habilidades: habilidadesApi});
        console.warn(this.state.habilidades);
      })
      .catch(error => {
        showToastTop("Não foi possível carregar as habilidades. O servidor está em manutenção. Tente novamente mais tarde.");
        //console.warn(error.message);
      });
    }

    parseJwt(token) {
      var base64Url = token.split(".")[1];
      var base64 = base64Url.replace("-", "+").replace("_", "/");
      return JSON.parse(Base64.atob(base64));
    }

    async init() {
      this.token = await AsyncStorage.getItem("token");
      this.idCandidato = this.parseJwt(this.token).sub;
      //console.warn(this.parseJwt(this.token).sub);
    }

 render(){
   const { selectedItems } = this.state;

   if(this.state.selectedItems.length > 5){
     this.state.selectedItems.pop();
   }

   return(
     <Root>
     <Content style={{backgroundColor: 'white'}}>
      <ScrollView style={styles.container}>

        <View style={{flexDirection: 'row'}}>
          <Text style={{fontSize: 16, fontWeight: 'bold',
              alignItems: 'flex-start'}}>Habilidade Técnicas</Text>
        </View>

        <View style={styles.lineStyle} />

         <View style={{ flex: 1}}>
         <View>
         {this.multiSelect? this.multiSelect.getSelectedItemsExt(this.state.selectedItems): null}
         </View>
         <View style={{marginTop: 30}}>
            <MultiSelect
              hideTags
              items={this.state.habilidades}
              uniqueKey="nome"
              ref={(component) => { this.multiSelect = component }}
              onSelectedItemsChange={this.onSelectedItemsChange}
              selectedItems={this.state.selectedItems}
              selectText="Selecione 5 habilidades"
              searchInputPlaceholderText="Search items..."
              onChangeInput={ (text)=> console.log(text)}
              tagRemoveIconColor="#CCC"
              tagBorderColor="rgb(0, 163, 151)"
              tagTextColor="rgb(0, 163, 151)"
              selectedItemTextColor="rgb(0, 163, 151)"
              selectedItemIconColor="rgb(0, 163, 151)"
              itemTextColor="#000"
              displayKey="nome"
              searchInputStyle={{ color: '#CCC' }}
              submitButtonColor="#ffffff"
              submitButtonText="confirmar"
            />
            </View>

          </View>

      </ScrollView>
    </Content>
    </Root>
   );
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    padding: 40,
  },
  lineStyle: {
    marginBottom: '3%',
    marginTop: '3%',
    borderBottomColor: 'rgb(0, 163, 151)',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center'
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    height: 40,
    borderColor: '#8a8c91',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(0, 163, 151)',
    marginTop: 20
  },
  titulo: {
    padding: 55,
  },
  lista: {
    height: 150,
  },
  containerLista:{
    flex: 1,
    margin: 15,
    padding: 30,
    flexDirection: 'row',
    //alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgb(0, 163, 151)',
  },
})
