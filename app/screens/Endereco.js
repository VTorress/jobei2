import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image, TextInput, AsyncStorage, Button, TouchableOpacity} from 'react-native';
import {Container, Content, Header, Body, Toast, Root, Picker, Form, Icon, Textarea} from 'native-base';

import axios from 'axios';
//import {getEnderecoApi} from '../config/restApi';
import MultiSelect from 'react-native-multiple-select';
import { habilidades, cargos } from '../config/data';
import {showToastTop, showToastBottom} from '../utils/helpers';

export default class EnderecoCadastro extends Component {

  constructor(props){
    super(props);

    this.state = {
      cep: '',
      cepError: false,
      numero: '',
      numeroError: false,
      endereco: '',
      enderecoError: false,
    }
  }

  onValueChange(value: string) {
     this.setState({
       cargo: value
     })
  }

  validate(texto, type){
    cep = /^(?=.{8,8}$)/
    number = /^[0-9\b]+$/
    if(type == 'cep'){
      if(cep.test(texto)){
        this.setState({
          cepError: false,
        })

      }
      else{
        this.setState({
          cepError: true,
        })
        showToastTop('CEP Inválido');
      }
    }
    else if(type == 'numero'){
      if(number.test(texto)){
        this.setState({
          numeroError: false,
        })

      }
      else{
        this.setState({
          numeroError: true,
        })
        showToastTop('Apenas números são permitidos');
      }
    }
  }

  getEndereco = (cep, numero) => {
    axios.get('https://viacep.com.br/ws/'+cep+'/json/')
    .then(res => {
      enderecoJSON = res.data;
      if (enderecoJSON.logradouro === undefined){
        this.setState({enderecoError: true});
        showToastTop('Não foi possível encotrar o endereço com este CEP. Tente novamente.');
      } else {
        this.setState({enderecoError: false});
        this.setState({endereco: enderecoJSON.logradouro + ", " + numero +
          " - " + enderecoJSON.localidade +  ", " + enderecoJSON.uf});
     }
    })
    .catch(error => {
      showToastTop("Não foi possível localizar o endereço. O servidor está em manutenção. Tente novamente mais tarde");
      //console.warn(error.message);
    });
  }

  goToScreen = (user) => {
      if(this.state.cepError === false && this.state.numeroError === false &&
        this.state.enderecoError === false){

        this.props.navigation.navigate('DadosProfissionais', {
          foto: user.foto,
          nome: user.nome,
          sobrenome: user.sobrenome,
          email: user.email,
          senha: user.senha,
          avatarSource: user.avatarSource,
          telefone: user.telefone,
          endereco: this.state.endereco});
      } else {
        showToastTop('Preencha os dados corretamente antes de continuar');
      }
    }


  render(){
      const cargosOrdenados = cargos.sort(function(a, b){
          return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
      });

      return(
        <Root>
          <ScrollView style={styles.container}>
              <View style={styles.titulo}>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>Endereço</Text>

                <View style={{flexDirection: 'row', marginLeft: '55%'}}>
                  <TouchableOpacity onPress = {() => this.goToScreen(this.props.navigation.state.params)}>
                  <Text style={{fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(0, 163, 151)'}}>continue</Text>
                  </TouchableOpacity>
                </View>
              </View>

            <View style={styles.lineStyle} />

            <View style={styles.inputContainer}>

              <TextInput placeholder='cep' style={[styles.inputs,
               this.state.cepError? styles.erroBorder: null]}
                onChangeText={(cep) => this.setState({cep})}
                onEndEditing={() => this.validate(this.state.cep, 'cep')}
                value={this.state.cep}
                autoCapitalize='none' />

              <TextInput placeholder='numero' style={[styles.inputs,
               this.state.numeroError? styles.erroBorder: null]}
                onChangeText={(numero) => this.setState({numero})}
                onEndEditing={() => this.validate(this.state.numero, 'numero')}
                value={this.state.numero}
                keyboardType={'number-pad'}
                />

                <TouchableOpacity onPress = {() => this.getEndereco(this.state.cep, this.state.numero)}>
                  <View style={{borderColor: 'rgb(0, 163, 151)', borderRadius: 5, borderWidth: 1,
                  marginTop: 20, width: '90%'}}>
                    <Text style={{fontSize: 12, padding: 10, height: 40, fontWeight: 'bold', color: 'rgb(0, 163, 151)',
                    textAlign: 'center', paddingBottom: 5}}>
                      BUSCAR ENDEREÇO
                    </Text>
                  </View>
                </TouchableOpacity>


              <TextInput placeholder='endereço' style={styles.inputs}
                onChangeText={texto => this.setState({endereco: texto})}
                autoCapitalize='none'
                editable={false}
                value={this.state.endereco}
                />

            </View>
          </ScrollView>
          </Root>
      );
  }

}
  const styles = StyleSheet.create({
    container: {
      //flex: 1,
      //justifyContent: 'center',
      //alignItems: 'center',
      padding: 40,
      backgroundColor: 'white'
    },
    lineStyle: {
      //marginTop: 20,
      //marginBottom: 20,
      marginTop: '3%',
      borderBottomColor: 'rgb(0, 163, 151)',
      borderBottomWidth: 1
    },
    inputContainer: {
      alignItems: 'center'
    },
    inputs: {
      paddingBottom: 5,
      marginTop: 20,
      height: 40,
      borderColor: '#8a8c91',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      width: '90%',
      borderRadius: 5,
      textAlign: 'center',
    },
    botao: {
      backgroundColor: 'rgb(0, 163, 151)',
      marginTop: 20
    },
    titulo: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    lista: {
      height: 150,
    },
    containerLista:{
      flex: 1,
      margin: 15,
      padding: 30,
      flexDirection: 'row',
      //alignItems: 'center',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: 'rgb(0, 163, 151)',
    },
    erroBorder:{
      borderWidth: 1,
      borderColor: 'red',
    },
    textArea: {
      marginTop: 20,
      borderColor: '#8a8c91',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      height: 120,
      borderRadius: 5

    },

  });
