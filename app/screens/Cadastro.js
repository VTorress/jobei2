import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image, TextInput, AsyncStorage, Button, TouchableOpacity, PixelRatio, Alert } from 'react-native';
import { Container, Content, Header, Body, Toast, Root, Picker, Form, Icon, Textarea } from 'native-base';
import axios from 'axios';
import { habilidades, cargos, imagem_padrao } from '../config/data';
import { onSignIn } from "../config/auth";
import AppIntroSlider from 'react-native-app-intro-slider';
import ImagePicker from 'react-native-image-picker';
import {showToastBottom, showToastTop} from '../utils/helpers';


export default class Cadastro extends Component {

  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      nomeError: false,
      sobrenome: '',
      sobrenomeError: false,
      email: '',
      emailError: false,
      senha: '',
      senhaError: false,
      avatarSource: null,
      foto: null
    }
    this.selectedPhotoTapped = this.selectedPhotoTapped.bind(this);
  }

  selectedPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 90,
      maxHeight: 90,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.warn('Response = ', response);

      if (response.didCancel) {
        console.warn('User cancelled image picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({ foto: 'data:image/jpeg;base64,' + response.data });
        this.setState({
          avatarSource: source,
        });
      }
    });
  }

  _renderItem = props => (
    <Text>{props.title}</Text>
);

  goToScreen = (screenName) => {
    this.props.navigation.navigate(screenName);
  }

  onValueChange(value: string) {
    this.setState({
      cargo: value
    })
  }

  onLearnMore = () => {

    if(this.state.nomeError === false && this.state.sobrenomeError === false
     && this.state.emailError === false && this.state.senhaError === false) {
    if(this.state.foto === null){
      //console.warn('NAO TEM FOOOOOOTOOOOOO');
      let source = {uri: imagem_padrao.foto };
      this.setState({avatarSource: source});
      this.setState({foto: imagem_padrao.foto});
    }

    this.props.navigation.navigate('DadosPessoais',
      {
        nome: this.state.nome,
        sobrenome: this.state.sobrenome,
        email: this.state.email,
        senha: this.state.senha,
        cargo: this.state.cargo,
        descricao: this.state.descricao,
        foto: this.state.foto,
        avatarSource: this.state.avatarSource,
      })
    } else {
     showToastTop('Preencha os dados corretamente antes de continuar');
    }
  };

  format(x) {
    return { nome: x.nome }
  }

  validate(texto, type) {
    alph = /^[a-zA-Z]+$/
    email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    senha = /^(?=.{6,}$)/

    if (type == 'nome') {
      if (alph.test(texto)) {
        this.setState({
          nomeError: false,
        })
      }
      else {
        this.setState({
          nomeError: true,
        })
        showToastBottom('Nome inválido');
      }
    }
    else if (type == 'sobrenome') {
      if (alph.test(texto)) {
        this.setState({
          sobrenomeError: false,
        })

      }
      else {
        this.setState({
          sobrenomeError: true,
        })
        showToastBottom('Sobrenome inválido');
      }0
    }
  }

  render() {

    const cargosOrdenados = cargos.sort(function (a, b) {
      return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
    });

    return (
      //<Content style={{backgroundColor: 'white'}}>
    <Root>
      
        <View style={styles.container}>
        <View style={styles.titulo}>
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Dados Pessoais</Text>

          <View style={{ flexDirection: 'row', marginLeft: '38%' }}>
            <TouchableOpacity onPress={() => this.onLearnMore(this.props.navigation.state.params)}>
              <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(0, 163, 151)' }}>continue</Text>
            </TouchableOpacity>
          </View>

        </View>

            <View style={styles.lineStyle} />

        <View style={{ marginTop: '10%', marginBottom: '5%', alignItems: 'center' }}>
          <TouchableOpacity onPress={this.selectedPhotoTapped.bind(this)}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                { marginBottom: 10 },
              ]}
            >
              {this.state.avatarSource === null ? (
                <Text style={{ fontSize: 12 }}>Selecione a Foto</Text>
              ) : (
                  <Image style={styles.avatar} source={this.state.avatarSource} />
                )}
            </View>
          </TouchableOpacity>
        </View>


        <View style={styles.inputContainer}>
          <TextInput placeholder='Nome' style={[styles.inputs,
          this.state.nomeError ? styles.erroBorder : null]}
            onChangeText={(nome) => this.setState({ nome })}
            onEndEditing={() => this.validate(this.state.nome, 'nome')}
            value={this.state.nome} />

          <TextInput placeholder='Sobrenome' style={[styles.inputs,
          this.state.sobrenomeError ? styles.erroBorder : null]}
            onChangeText={(sobrenome) => this.setState({ sobrenome })}
            value={this.state.sobrenome}
            onEndEditing={() => this.validate(this.state.sobrenome, 'sobrenome')}
          />

          {/* <TextInput placeholder='E-mail' style={[styles.inputs,
          this.state.emailError ? styles.erroBorder : null]}
            onChangeText={(email) => this.setState({ email })}
            value={this.state.email}
            autoCapitalize='none'
            onEndEditing={() => this.validate(this.state.email, 'email')}
          />

          <TextInput placeholder='Senha' style={[styles.inputs,
          this.state.senhaError ? styles.erroBorder : null]}
            onChangeText={(senha) => this.setState({ senha })}
            value={this.state.senha}
            autoCapitalize='none'
            secureTextEntry={true}
            onEndEditing={() => this.validate(this.state.senha, 'senha')}
          /> */}

        </View>
      </View>
      {/* </ScrollView> */}
      </Root>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    backgroundColor: 'white',
  },
  lineStyle: {
    marginTop: '3%',
    borderBottomColor: 'rgb(58, 203, 198)',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center',
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    //borderColor: '#8a8c91',
    borderColor: '#9B9B9B',
    //borderColor: 'rgb(58, 203, 198)',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  },
  titulo: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  erroBorder: {
    borderWidth: 1,
    borderColor: 'red',
  },
  erroText: {
    color: 'red',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
});
