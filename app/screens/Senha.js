import React, { Component } from 'react';
import axios from 'axios';
import { apiURI } from '../config/JobeiApi';
import { showToastTop } from '../utils/helpers';
import { Root, Content } from 'native-base';
import { Form, TextValidator } from 'react-native-validator-form';
import { Platform, StyleSheet, Text, View, ScrollView, Image, TextInput, AsyncStorage, Button, TouchableOpacity, PixelRatio, Alert } from 'react-native';

const idCandidato = "";

export default class Senha extends Component {

    constructor(props) {
        super(props);

        this.state = {
            senha: '',
            senhaError: false,
            confirmarSenha: '',
            user: {},
        };

        this.handlePassword = this.handlePassword.bind(this);
        this.handleRepeatPassword = this.handleRepeatPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
      Form.addValidationRule('isPasswordMatch', (value) => {
        if(value !== this.state.senha) {
          return false;
        }
        return true;
      });
    }

    postSenha = (user) => {
        axios.post(apiURI + '/candidatos/' + this.idCandidato + '/alterarSenha',
            {
                senha: user.senha
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'api-version': '1.0',
                    'Authorization': 'Bearer ' + this.token
                }
            })
            .then(res => {
                showToastTop('Senha alterada com sucesso!');
                this.props.navigation.navigate('Perfil');
            })
            .catch(error => {
                showToastTop('Não foi possível alterar sua senha. Tente novamente mais tarde')
            })

    }

    handlePassword(event) {
        const {user} = this.state;
        user.senha = event.nativeEvent.text;
        //isso ta certo?
        this.setState({ senha });
    }

    handleRepeatPassword(event) {
        const {user} = this.state;
        user.confimarSenha = event.nativeEvent.text;
        //isso ta certo?????? AAAAAA
        this.setState({ confimarSenha });
    }


    handleSubmit() {
        this.refs.form.submit();
    }

    render() {

        return (
           
           <Content style={{backgroundColor: 'white'}}>

                <View style={styles.inputContainer}>
                <Text style={styles.item}>SENHA ATUAL</Text>
          <TextInput placeholder='Senha Atual' style={[styles.inputs,
          this.state.nomeError ? styles.erroBorder : null]}
          
            
            value={this.state.senha} />
            
            <Text style={styles.item}>SENHA NOVA</Text>
          <TextInput placeholder='Senha Nova' style={[styles.inputs,
          this.state.sobrenomeError ? styles.erroBorder : null]}
           
            value={this.state.confirmarSenha}
          
          /> 
            <TextInput placeholder='Confimar Senha' style={[styles.inputs,
          this.state.sobrenomeError ? styles.erroBorder : null]}
           
            value={this.state.confirmarSenha}
          
          />

          </View>

            <View style={styles.inputContainer2}>
 <Button
              style={styles.botao}
              color='rgb(0, 163, 151)'
              title='ALTERAR'
            />

            </View>
         

               

                
                </Content>

            
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 40,
      backgroundColor: 'white',
    },
    lineStyle: {
      marginTop: '3%',
      borderBottomColor: 'rgb(58, 203, 198)',
      borderBottomWidth: 1
    },
    inputContainer: {
      alignItems: 'flex-start',
      marginLeft: 35
    },

    inputContainer2: {
        alignItems: 'center',
        marginTop: 40
      },
    inputs: {
      paddingBottom: 5,
      marginTop: 10,
      textAlign: 'center',
      height: 40,
      //borderColor: '#8a8c91',
      borderColor: '#9B9B9B',
      //borderColor: 'rgb(58, 203, 198)',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      width: '90%',
      borderRadius: 5
    },
    botao: {
      backgroundColor: 'rgb(58, 203, 198)',
      marginTop: 20,
      
    },
    titulo: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    erroBorder: {
      borderWidth: 1,
      borderColor: 'red',
    },
    erroText: {
      color: 'red',
    },
    avatarContainer: {
      borderColor: '#9B9B9B',
      borderWidth: 1 / PixelRatio.get(),
      justifyContent: 'center',
      alignItems: 'center',
    },
    avatar: {
      borderRadius: 50,
      width: 100,
      height: 100,
    },
    item: {
        color: "rgb(0, 163, 151)",
         fontSize:14,
         marginTop:40,
         
      },

})