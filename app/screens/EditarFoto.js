import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image, TextInput, AsyncStorage, Button, TouchableOpacity, PixelRatio, Alert } from 'react-native';
import { Container, Content, Header, Body, Toast, Root, Picker, Form, Icon, Textarea } from 'native-base';

import { habilidades, cargos, imagem_padrao } from '../config/data';
import { onSignIn } from "../config/auth";
import AppIntroSlider from 'react-native-app-intro-slider';
import ImagePicker from 'react-native-image-picker';
import {showToastBottom, showToastTop} from '../utils/helpers';

import Base64 from "../config/Base64";

import axios from "axios";
import {apiURI} from '../config/JobeiApi';

const token = "";
const idCandidato = "";

export default class EditarFoto extends Component {

  constructor(props) {
    super(props);

    this.state = {
      avatarSource: null,
      foto: null
    }

    this.selectedPhotoTapped = this.selectedPhotoTapped.bind(this);
  }

  selectedPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 90,
      maxHeight: 90,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.warn('Response = ', response);

      if (response.didCancel) {
        console.warn('User cancelled image picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({ foto: 'data:image/jpeg;base64,' + response.data });
        this.setState({
          avatarSource: source,
        });
      }
    });
  }

  async componentDidMount() {
    await this.init();
    let perfilApi = null;

    axios.get( apiURI + '/candidatos/' + this.idCandidato,
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + this.token
    }})
    .then(res => {
      perfilApi = res.data;
      let source = { uri: perfilApi.imagem };
      this.setState({foto: perfilApi.imagem});
      this.setState({avatarSource: source});
    })
    .catch(error => {
      this.showToast('Não foi possível carregar os dados do usuário. Tente novamente mais tarde');
    });

  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
    //console.warn(this.parseJwt(this.token).sub);
  }

  _renderItem = props => (
    <Text>{props.title}</Text>
  );

  goToScreen = (screenName) => {
    this.props.navigation.navigate(screenName);
  }

  onLearnMore = () => {

    if(this.state.nomeError === false && this.state.sobrenomeError === false
     && this.state.emailError === false && this.state.senhaError === false) {
    if(this.state.foto === null){
      //console.warn('NAO TEM FOOOOOOTOOOOOO');
      let source = {uri: imagem_padrao.foto };
      this.setState({avatarSource: source});
      this.setState({foto: imagem_padrao.foto});
    }

    this.props.navigation.navigate('DadosPessoais',
      {
        nome: this.state.nome,
        sobrenome: this.state.sobrenome,
        email: this.state.email,
        senha: this.state.senha,
        cargo: this.state.cargo,
        descricao: this.state.descricao,
        foto: this.state.foto,
        avatarSource: this.state.avatarSource,
      })
    } else {
     showToastTop('Preencha os dados corretamente antes de continuar');
    }
  };

  render() {

    return (
      //<Content style={{backgroundColor: 'white'}}>
    <Root>
      <View style={styles.container}>

        <View style={styles.titulo}>
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Foto</Text>
        </View>

        <View style={styles.lineStyle} />

        <View style={{ marginTop: '10%', marginBottom: '5%', alignItems: 'center' }}>
          <TouchableOpacity onPress={this.selectedPhotoTapped.bind(this)}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                { marginBottom: 10 },
              ]}
            >
              {this.state.avatarSource === null ? (
                <Text style={{ fontSize: 12 }}>Selecione a Foto</Text>
              ) : (
                  <Image style={styles.avatar} source={this.state.avatarSource} />
                )}
            </View>
          </TouchableOpacity>
        </View>
        <Text style={{marginTop: 20}}>clique na foto para editar</Text>
      </View>
    </Root>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    backgroundColor: 'white',
  },
  lineStyle: {
    marginTop: '3%',
    borderBottomColor: 'rgb(58, 203, 198)',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center',
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    //borderColor: '#8a8c91',
    borderColor: '#9B9B9B',
    //borderColor: 'rgb(58, 203, 198)',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  },
  titulo: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  erroBorder: {
    borderWidth: 1,
    borderColor: 'red',
  },
  erroText: {
    color: 'red',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
});
