import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image, TouchableOpacity, PixelRatio} from 'react-native';
import {Container, Content, List, ListItem, Icon, Button, Thumbnail, Badge, Root} from 'native-base';

import axios from 'axios';
import { onSignIn } from "../config/auth";
import {apiURI} from '../config/JobeiApi';
import {showToastTop, showToastBottom} from '../utils/helpers';
export default class FinalCadastro extends Component {

  constructor(props){
    super(props);
  }

  postCadastro(user){
    var habilidades = [];
    var valores = [];
    user.habilidades.forEach(x => habilidades.push({nome: x}));
    user.valores.forEach(x => valores.push({nome: x}));

    axios.post(apiURI + '/candidatos',
    {
      nome: user.nome,
      sobrenome: user.sobrenome,
      email: user.email,
      senha: user.senha,
      endereco: user.endereco,
      descricao: user.descricao,
      cargo: user.cargo,
      telefone: user.telefone,
      endereco: user.endereco,
      habilidades: habilidades,
      valores: valores,
      imagem: user.foto,
    },
    {
      headers: {
        'Content-Type' : 'application/json',
        'api-version':'1.0'
    }})
    .then(res =>{
      //console.warn(res);
      showToastTop('usuário cadastrado com sucesso');
      this.props.navigation.navigate('Login');
    })
    .catch(error =>{
      //console.warn(error.message);
      showToastTop('Não foi possível concluir o cadastro. Tente novamente mais tarde');
    })
  }

  render() {

   const { foto, nome, sobrenome, email, senha, avatarSource, cargo, descricao, endereco, habilidades, valores} = this.props.navigation.state.params;
   //const { selectedItems } = this.state;

   return(
     <Root>
    <Content style={{backgroundColor: 'white', padding: 20}}>

      <TouchableOpacity onPress = {() => this.postCadastro(this.props.navigation.state.params)}>
        <Text style={{fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(0, 163, 151)'}}>cadastrar</Text>
      </TouchableOpacity>

     <List>
       <ListItem itemHeader first style={{alignItems: 'center'}}>
        <View
          style={[
            styles.avatar,
            styles.avatarContainer,
            { marginBottom: 10 },
          ]}
          >
          {avatarSource === null ? (
            <Text style={{fontSize: 12}}>select a photo</Text>
          ) : (
            <Image style={styles.avatar} source={avatarSource} />
          )}
          </View>



         <View style={{flexDirection: 'column'}}>
         <Text style={{fontWeight: 'bold', fontSize: 16, marginLeft: 20}}>{nome + ' ' + sobrenome}</Text>
         <Text style={{marginLeft: 20}}>{cargo}</Text>
         </View>
       </ListItem>

       <ListItem itemHeader>
        <Text style={{color: 'rgb(0, 163, 151)'}}>DESCRIÇÃO</Text>
       </ListItem>

        <ListItem>
          <Text>{descricao}</Text>
        </ListItem>

       <ListItem itemHeader>
         <Text style={{color: 'rgb(0, 163, 151)'}}>DADOS</Text>
       </ListItem>

       <ListItem>
         <Text>{endereco}</Text>
       </ListItem>
       <ListItem>
         <Text>{telefone}</Text>
       </ListItem>
       <ListItem>
         <Text>{email}</Text>
       </ListItem>

       <ListItem itemHeader>
         <View style={{flexDirection: 'row'}}>
         <Text style={{color: 'rgb(0, 163, 151)'}}>HABILIDADES</Text>
         <Text style={{color: 'rgb(0, 163, 151)', marginLeft: 60}}>VALORES</Text>
         </View>
       </ListItem>

       <ListItem>
         <View style={{flexDirection: 'row'}}>
           <View>
           {habilidades.map(habilidade =>
             <Badge
               style={{
                 backgroundColor: 'rgb(0, 163, 151)',
                 borderBottomLeftRadius: 5,
                 borderBottomRightRadius: 5,
                 borderTopLeftRadius: 5,
                 borderTopRightRadius: 5,
                 marginTop: 5,
                 flexDirection: 'row'}}>
                 <Text style={{color: 'white'}}>{habilidade}</Text>
             </Badge>
           )}
           </View>

           <View style={{marginLeft: 60}}>
             {valores.map(valor =>
             <Badge
               style={{
                 backgroundColor: 'rgb(0, 163, 151)',
                 borderBottomLeftRadius: 5,
                 borderBottomRightRadius: 5,
                 borderTopLeftRadius: 5,
                 borderTopRightRadius: 5,
                 marginTop: 5,
                 flexDirection: 'row'}}>
                 <Text style={{color: 'white'}}>{valor}</Text>
             </Badge>
           )}
           </View>
        </View>
       </ListItem>

     </List>
   </Content>
   </Root>
   );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 55,
    flexDirection: 'row',
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center'
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    height: 40,
    borderColor: '#8a8c91',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(0, 163, 151)',
    marginTop: 20
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height:100,
  },
});
