import React, { Component } from 'react';

import {
  Platform, StyleSheet, Text, View, ScrollView, Image,
  TextInput, Button, AsyncStorage, TouchableOpacity
} from 'react-native';

import { Container, Content, Header, Body, Toast, Root } from 'native-base';

import { onSignIn } from "../config/auth";

import axios from 'axios';
import {apiURI} from '../config/JobeiApi';

export default class Login extends Component {


  constructor(props) {
    super(props);

    this.logar = this.logar.bind(this);

    this.state = {
      email: '',
      senha: '',
      emailError: false,
      senhaError: false,
    }
  }

  _storeData = async (data) => {
    try {
      await AsyncStorage.setItem('token', JSON.stringify(data));
      onSignIn(token.accessToken);
    } catch (error) {
      console.warn('um erro ocorreu');
    }
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
      // console.warn(value);
      if (value !== null) {
        this.props.navigation.navigate('UserLogedIn');
      }
    } catch (error) {
      alert('um erro ocorreu');
    }
  }

  showToast(message) {
    return (
      Toast.show({
        text: message,
        buttonText: 'OK',
        duration: 5000,
        position: 'bottom',
        textStyle: {color: 'white'},
        type: 'warning'
      })
    );
  }

  validate(texto, type) {
    email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    senha = /^(?=.{6,}$)/

    if (type == 'email') {
      if (email.test(texto)) {
        this.setState({
          emailError: false,
        })
      }
      else {
        this.setState({
          emailError: true,
        })
        this.showToast('Entre com um email válido');
      }
    }
    else if (type == 'senha' && this.state.senha.length > 0) {
      if (senha.test(texto)) {
        this.setState({
          senhaError: false,
        })
      }
      else {
        this.setState({
          senhaError: true,
        })
        this.showToast('A senha deve conter mais de 6 caracteres');
      }
    }
  }

  getToken(email, senha) {
    //axios.post('http://192.168.4.165:5000/api/auth',
    axios.post(apiURI + '/auth',
      {
        email: email,
        senha: senha
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'api-version': '1.0',
          //'Authorization': 'Bearer ' + JSON.stringify(token)
        }
      })
      .then(res => {
        token = res.data,
          //alert(token.accessToken);
          this._storeData(token.accessToken);
        this._retrieveData();
      })
      .catch(error => {
        console.warn(error.message);
      });
  }

  logar() {
    //const name = (event.target) && (event.target.name);
    //const value = (event.target) && (event.target.value);
    var user_email = this.state.email;
    var user_senha = this.state.senha;
    this.getToken(user_email, user_senha);

  }

  home() {
    this._storeData('123456');
    this._retrieveData();
  }

  findCoordinates = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);
        this.setState({ location });
        console.warn(location);
      },
      error => console.warn(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000 }
    );
  };


  render() {
    return (
    <Root>
      <Content style={{ backgroundColor: 'white' }}>
        <ScrollView style={styles.container}>

          <Image source={require('./../resources/img/logo.png')}
            style={{ width: 260, height: 120, marginTop: 5, alignItems: 'center' }} />

          <View style={styles.lineStyle} />

          <Text
            style={{ fontSize: 27, textAlign: 'center', marginTop: 30, marginBottom: 20 }}>
            Login
          </Text>

          <View style={styles.inputContainer}>
            <TextInput placeholder='email' style={[styles.inputs,
            this.state.emailError ? styles.erroBorder : null]}
              onChangeText={(email) => this.setState({ email })}
              onEndEditing={() => this.validate(this.state.email, 'email')}
              autoCapitalize='none'
              value={this.state.email} />

            <TextInput placeholder='Senha' style={[styles.inputs,
            this.state.senhaError ? styles.erroBorder : null]}
              onChangeText={(senha) => this.setState({ senha })}
              value={this.state.senha}
              autoCapitalize='none'
              secureTextEntry={true}
              onEndEditing={() => this.validate(this.state.senha, 'senha')}
            />


            <View style={{ margin: 10 }} />

            {/* <Text
              style={{ textAlign: 'center', marginBottom: 30, color: 'rgb(0, 163, 151)' }}>
              esqueci minha senha</Text> */}
            <Button
              onPress={() => { this.logar() }}
              style={styles.botao}
              color='rgb(0, 163, 151)'
              title='ENTRAR'
            />
            <Button
              onPress={() => { this.home() }}
              style={styles.botao}
              color='rgb(0, 163, 151)'
              title='home (sem login)'
            />
          </View>

          <View style={styles.lineStyle} />


          <View style={alignItems = 'center'}>

            <View style={{ flexDirection: 'row', marginLeft: '10%', marginTop: 7}}>
              <Text> Não tem uma conta? </Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Cadastro')}>
                <Text style={{color: 'rgb(0, 163, 151)' }}>Cadastre-se</Text>
              </TouchableOpacity>
            </View>

            {/* <Text
              style={{ marginTop: 10, textAlign: 'center' }}>
              Não tem uma conta?
                    </Text>
            <Button
              onPress={() => this.props.navigation.navigate('Cadastro')}
              style={styles.botao}
              color='rgb(0, 163, 151)'
              title='Cadastre-se'
            />
            <Button
              onPress={() => this.findCoordinates()}
              style={styles.botao}
              color='rgb(0, 163, 151)'
              title='position'
            /> */}
          </View>
        </ScrollView>
      </Content>
    </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 55,
    flexDirection: 'row',
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center',
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    borderColor: '#8a8c91',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5,
  },
  botao: {
    backgroundColor: 'white',
    marginTop: 20
  },
  erroBorder: {
    borderWidth: 1,
    borderColor: 'red',
  },
});
